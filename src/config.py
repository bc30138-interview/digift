import os


class Config:
    SECRET_KEY = os.environ.get("SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
    CELERY_BROKER_URL = os.environ.get("RABBITMQ_URI")
    CELERY_BACKEND = os.environ.get("CELERY_BACKEND")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
