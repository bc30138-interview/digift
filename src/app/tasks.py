from cryptography.fernet import Fernet
from celery import Celery
from .models import Encrypted
from . import db

celery = Celery(__name__, autofinalize=False)


@celery.task(name='celery_app.encode')
def encode(data: bytes, key: str, uuid: str, user_id: str):
    f = Fernet(key.encode('UTF-8'))
    encrypted_data = f.encrypt(data)
    encrypted_object = Encrypted(
        user_id=user_id,
        uuid=uuid,
        file=encrypted_data.decode('UTF-8')
    )
    db.session.add(encrypted_object)
    db.session.commit()
    return uuid
