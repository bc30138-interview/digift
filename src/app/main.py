import io
from uuid import uuid4

from flask.json import jsonify
from flask import Blueprint, request, send_file
from cryptography.fernet import Fernet

from .models import Encrypted, Original
from .tasks import encode
from .auth import token_required
from . import db

main = Blueprint('main', __name__)


@main.route('/file/<uuid>', methods=['GET'])
@token_required
def get_file(current_user, uuid):
    encrypted = Encrypted.query.filter_by(uuid=uuid).first()
    if not encrypted:
        return jsonify(
            {
                "message": "Not found"
            }
        ), 404
    if encrypted.user_id != current_user.id:
        return jsonify(
            {
                "message": "File not belongs to you"
            }
        ), 401
    f = Fernet(current_user.key.encode('UTF-8'))
    result = f.decrypt(encrypted.file.encode('UTF-8'))
    return send_file(
        io.BytesIO(result),
        attachment_filename="encrypted.bin"
    )


@main.route('/original/<uuid>', methods=['GET'])
@token_required
def get_original(current_user, uuid):
    original = Original.query.filter_by(uuid=uuid).first()
    if not original:
        return jsonify(
            {
                "message": "Not found"
            }
        ), 404
    if original.user_id != current_user.id:
        return jsonify(
            {
                "message": "File not belongs to you"
            }
        ), 401
    return send_file(
        io.BytesIO(original.file),
        attachment_filename="original.bin"
    )


@main.route('/file', methods=['GET', 'POST'])
@token_required
def upload(current_user):
    if request.method == 'POST':
        if request.mimetype == 'application/octet-stream':
            return send_task(request.data, current_user.key, current_user.id)
        elif request.mimetype == 'multipart/form-data':
            print(request.files['file'].filename.split('.')[-1])
            return send_task(request.files['file'].read(), current_user.key, current_user.id)
    else:
        user_files = Encrypted.query.filter_by(user_id=current_user.id).with_entities(Encrypted.uuid)
        result_dict = [{'uuid': u[0]} for u in user_files.all()]
        return jsonify(result_dict)


def send_task(data: bytes, key: str, user_id: int):
    job_uuid = uuid4()

    original_object = Original(
        user_id=user_id,
        uuid=job_uuid,
        file=data
    )
    db.session.add(original_object)
    db.session.commit()

    encode.delay(data, key, job_uuid, user_id)

    return jsonify(
        {
            "uuid": job_uuid
        }
    ), 200
