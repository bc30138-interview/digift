import jwt
from functools import wraps
from flask import Blueprint, request, current_app
from flask.json import jsonify
from cryptography.fernet import Fernet

from .models import User
from . import db

auth = Blueprint('auth', __name__)


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'])
            current_user = User.query.filter_by(name=data['username']).first()
        except:
            return jsonify({'message': 'Token is invalid!'}), 401

        return f(current_user, *args, **kwargs)

    return decorated


@auth.route('/login', methods=['GET'])
def get_token():
    auth = request.authorization
    if not auth or not auth.username:
        return jsonify(
            {
                "message": "Could not verify"
            }
        ), 401

    user = User.query.filter_by(name=auth.username).first()

    if not user:
        key = Fernet.generate_key()
        token = jwt.encode(
            {'username': auth.username},
            current_app.config['SECRET_KEY']
        )
        user = User(
            key=key.decode('UTF-8'),
            name=auth.username,
            token=token.decode('UTF-8')
        )
        db.session.add(user)
        db.session.commit()

    return jsonify({'token': user.token})
