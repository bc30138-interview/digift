from sqlalchemy.sql.schema import ForeignKey
from . import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(1000))
    name = db.Column(db.String(1000))
    token = db.Column(db.String(1000))


class Encrypted(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, ForeignKey('user.id'))
    uuid = db.Column(db.String(1000))
    file = db.Column(db.String)


class Original(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, ForeignKey('user.id'))
    uuid = db.Column(db.String(1000))
    file = db.Column(db.LargeBinary)
