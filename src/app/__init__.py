from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app(config_class):
    return entrypoint(config_class=config_class, mode='app')


def create_celery(config_class):
    return entrypoint(config_class=config_class, mode='celery')


def entrypoint(config_class, mode):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)

    from .models import User, Encrypted

    with app.app_context():
        db.create_all()

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .tasks import celery

    configure_celery(app, celery)

    if mode == 'app':
        return app
    elif mode == 'celery':
        return celery


def configure_celery(app, celery):
    celery.conf.broker_url = app.config['CELERY_BROKER_URL']
    celery.conf.result_backend = app.config['CELERY_BACKEND']
    celery.conf.task_serializer = 'pickle'
    celery.conf.result_serializer = 'pickle'
    celery.conf.accept_content = ['pickle']

    TaskBase = celery.Task

    class AppContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = AppContextTask

    celery.finalize()
