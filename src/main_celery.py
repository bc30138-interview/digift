import celery
from app import create_celery
from config import Config

celery = create_celery(Config)
